'use strict';
// LOGIN CONTROLLER
// Description: Define the following functionalities:
// Making service calls to login a user

routerApp.controller('login', function($scope, $location, authenticate, session) {
    $scope.submitForm = function() {
        authenticate.login($scope.username, $scope.password, function(response) {
            if(response.authentication == "success") {
                session.set("user", $scope.username);
                session.set("password", $scope.password);
                $location.path("/root/work");
                alert("Succcessfully Logedin")
            }else{
                $scope.error = "User ID or Password is Incorrect Please Check Chutiye";
                alert("Login details are incorrect")
            }
        });
    }
});